#!/usr/bin/env bash

alias emacs="/Applications/Emacs.app/Contents/MacOS/Emacs -nw"

# Set the default editor
export EDITOR=emacs
export VISUAL=emacs
