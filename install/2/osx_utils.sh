#!/usr/bin/env bash

echo -e "\n--- Install more recent versions of some OS X tools"
brew tap homebrew/dupes
brew install homebrew/dupes/grep
