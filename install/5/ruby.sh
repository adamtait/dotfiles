#!/usr/bin/env bash


echo -e "\n--- rbenv & Ruby 2.5.0"
rbenv install 2.5.0
rbenv global 2.5.0
